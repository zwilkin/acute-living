//
//  AccountVC.swift
//  Acute Living
//
//  Created by Zach on 12/8/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import Firebase
import PhoneNumberKit

class AccountVC: UIViewController, UITextFieldDelegate {

    // Placeholder Variables
    let EDIT: String = "Edit"
    let SAVE: String = "Save"
    let CANCEL: String = "Cancel"
    
    let phoneNumberKit = PhoneNumberKit()
    
    // Firebase Variables
    var auth: FIRAuth!
    var fbUser: FIRUser!
    var dbRef: FIRDatabaseReference!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.enableKeyboardDismissOnTap() // Cited in LoginVC
        
        // Setup Firebase Variables
        auth = FIRAuth.auth()
        fbUser = auth.currentUser!
        dbRef = FIRDatabase.database().reference()
        
        // Setup Input Field delegates
        for input in inputFields { input.delegate = self }
        
        // Set up the Editing Menu buttons
        toggleEditing(enabled: false)
        
        // Retrieve the user's information from the database
        let idRef = dbRef.child(FireHelper.USERS).child(fbUser.uid)
        idRef.observeSingleEvent(of: .value) { (snapshot) in
            self.input_firstName.text = snapshot.childSnapshot(forPath: FireHelper.FIRST).value as? String
            self.input_lastName.text = snapshot.childSnapshot(forPath: FireHelper.LAST).value as? String
            self.input_property.text = snapshot.childSnapshot(forPath: FireHelper.PROPERTY).value as? String
            self.input_unit.text = snapshot.childSnapshot(forPath: FireHelper.UNIT).value as? String
        
            // Parse PhoneNumber
            let phoneRaw = String(describing: Int((snapshot.childSnapshot(forPath: FireHelper.PHONE).value as? Double)!))
            do {
                let phoneParsed = try self.phoneNumberKit.parse(phoneRaw, withRegion: PhoneNumberKit.defaultRegionCode(), ignoreType: true)
                let phoneNumber = self.phoneNumberKit.format(phoneParsed, toType: PhoneNumberFormat.national, withPrefix: false)
                self.input_phoneNum.text = phoneNumber
            } catch {
                print("Generic Parsing Error")
            }
        }
    }
    
    
    // MARK: - Text Field Functionality
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Check if it is the last input field
        if textField.tag == (inputFields.count - 1) { textField.resignFirstResponder() } // "Done"
        // Move to the next input field
        else { inputFields[textField.tag + 1].becomeFirstResponder() }
        return false
    }

    // MARK: - Custom Methods
    func toggleEditing(enabled: Bool) {
        if enabled { // Currently Editing
            // Change 'Edit' to 'Save' and add 'Cancel'
            self.navigationItem.rightBarButtonItems = [
                UIBarButtonItem(title: CANCEL, style: .plain, target: self, action: #selector(AccountVC.menuButtonPress(sender:))),
                UIBarButtonItem(title: SAVE, style: .done, target: self, action: #selector(AccountVC.menuButtonPress(sender:)))
            ]
        }
        else { // Clicked Save
            self.navigationItem.rightBarButtonItems = [
                UIBarButtonItem(title: EDIT, style: .plain, target: self, action: #selector(AccountVC.menuButtonPress(sender:)))
            ]
        }
    }
    @objc func menuButtonPress(sender: UIBarButtonItem) {
        switch sender.title! {
        case EDIT:
            toggleEditing(enabled: true) // Turn on
            break
        case SAVE:
            // TODO: Verify changes with user
            let alert = UIAlertController(title: "Save Changes?", message: "Are you sure you wish to save these changes?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
                // TODO: Save user changes
                
                self.toggleEditing(enabled: false) // Turn Off
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            break
        case CANCEL:
            toggleEditing(enabled: false) // Turn Off
            break
        default: break
        }
    }
    
    
    // MARK: - Actions & Outlets
    // Outlets
    @IBOutlet var inputFields: [UITextField]!
    @IBOutlet weak var input_firstName: UITextField!
    @IBOutlet weak var input_lastName: UITextField!
    @IBOutlet weak var input_phoneNum: UITextField!
    @IBOutlet weak var input_property: UITextField!
    @IBOutlet weak var input_unit: UITextField!
    @IBOutlet weak var input_address: UITextView!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var modifyButton: UIButton!
    // Actions
    @IBAction func notificationsToggled(_ sender: UISwitch) {
        if sender.isOn {
            print("Push Notifications: Enabled")
        }
        else {
            print("Push Notifications: Disabled")
        }
    }
    
}
