//
//  ActionWindowVC.swift
//  Acute Living
//
//  Created by Zach on 12/8/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit


// Segue Identifiers
private let ACCOUNT_SEGUE: String = "AccountSegue"
private let BILLS_SEGUE: String = "BillsSegue"
private let MAINTENANCE_SEGUE: String = "MaintenanceSegue"

class ActionWindowVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        // Make sure NavBar is hidden on Action Window
        navigationController?.navigationBar.isHidden = true;
    }
    override func viewWillDisappear(_ animated: Bool) {
        // Make sure NavBar is shown on other windows
        navigationController?.navigationBar.isHidden = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func logOut(_ sender: Any) {
        // Verify with user before logging out
        let alert = UIAlertController(title: "Log Out?", message: "Are you sure you wish to log out?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (action) in
            self.performSegue(withIdentifier: LOGOUT_UNWIND, sender: sender)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionButton(_ sender: UIButton) {
        switch (sender.tag) {
        case 0: // Bills
            print("Navigating to Bills")
            performSegue(withIdentifier: BILLS_SEGUE, sender: sender)
            break
        case 1: // Maintenance
            print("Navigating to Maintenance")
//            performSegue(withIdentifier: MAINTENANCE_SEGUE, sender: sender)
            break
        case 2: // Account
            print("Navigating to My Account")
            performSegue(withIdentifier: ACCOUNT_SEGUE, sender: sender)
            break
        default: break
        }
    }
}
