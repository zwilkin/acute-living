//
//  Setup1.swift
//  Acute Living
//
//  Created by Zach on 12/13/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import Firebase

class Setup: UIViewController, UITextFieldDelegate {

    // Firebase Variables
    let auth: FIRAuth! = FIRAuth.auth()
    let database: FIRDatabase = FIRDatabase.database()
    var fbUser: FIRUser!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.enableKeyboardDismissOnTap()   // Cited in LoginVC
        for input in inputFields { input.delegate = self } // Set up TextField delegates
        fbUser = auth.currentUser   // Grab the logged in Firebase user
        
        // Get the Database reference for the specific user (by uId)
        let idRef = FIRDatabase.database().reference().child(FireHelper.USERS).child(fbUser.uid)
        
        // Populate the Unit # and Email labels
        idRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let unit: String = snapshot.childSnapshot(forPath: "unit").value as? String {
                let email: String = self.fbUser.email!
                self.unitLabel.text = unit
                self.emailLabel.text = email
            }
        })
    }
    
    // MARK: - Custom Methods
    public static func toggleActivityIndicator(on: Bool, indicator: UIActivityIndicatorView) {
        if on { indicator.startAnimating() }
        else { indicator.stopAnimating() }
    }
    
    // MARK: - Text Field Functionality
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Check if it is the last input field
        if textField.tag == (inputFields.count - 1) { textField.resignFirstResponder() } // "Done"
        // Move to the next input field
        else { inputFields[textField.tag + 1].becomeFirstResponder() }
        return false
    }
    
    // MARK: - Outlets & Actions
    // Outlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet var inputFields: [UITextField]!
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var verifyNewPass: UITextField!
    // Actions
    @IBAction func finishSetup(_ sender: Any) {
        // Check for valid internet conneciton before attempting any network operations
        let reachability = Reachability(hostname: "www.google.com")
        if reachability?.connection == Reachability.Connection.none {
            // No current internet connection - notify the user
            print("NO CONNECTION!!!")
            let alert = UIAlertController(title: "No Connection", message: "Connect to the internet and try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else {
            // Valid Connection - Verify details
            
            // Check that all necessary fields are filled in
            for input in inputFields {
                if (input.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")).isEmpty)! {
                    // A required field is empty - alert the user
                    let alert = UIAlertController(title: "Empty Fields", message:
                        "One of more of the required fields are empty.\nPlease make sure all fields are filled in.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            }
            // Check that both password fields are equal
            if newPass.text! != verifyNewPass.text! {
                // Alert the user
                let alert = UIAlertController(title: "Password Mismatch", message: "Passwords do not match. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            // Create the account updates
            let first: String = inputFields[0].text!
            let last: String = inputFields[1].text!
            let phone: Double = Double(inputFields[2].text!)!
            let password: String = inputFields[3].text!
            let setupUpdates: [String: Any] = ["firstName": first, "lastName": last, "phoneNumber": phone, "setupComplete": true]
            
            // Get the database references for "users"
            let usersRef: FIRDatabaseReference = FIRDatabase.database().reference().child(USERS).child(fbUser.uid)
            
            // Send the new updates to the database
            Toggle.activityIndicator(on: true, indicator: self.activityIndicator)
            // Update Password
            fbUser.updatePassword(password) { (error) in
                if let e = error {
                    let alert = UIAlertController(title: "Error", message: "There was an error updating your password.\nPlease try again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    alert.present(alert, animated: true, completion: nil)
                    print("\n" + e.localizedDescription)
                }
                else {
                    // Update Account Data
                    usersRef.updateChildValues(setupUpdates, withCompletionBlock: { (error, reference) in
                        if let e = error {
                            let alert = UIAlertController(title: "Error", message: "There was an error updating your account.\nPlease try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            alert.present(alert, animated: true, completion: nil)
                            print("\n" + e.localizedDescription)
                        }
                        else {
                            Toggle.activityIndicator(on: false, indicator: self.activityIndicator)
                            // Segue to Action Window (Dashboard) - (i.e. Log In)
                            self.performSegue(withIdentifier: SETUP_TO_DASHBOARD_SEGUE, sender: sender)
                        }
                    })
                }
            }
        }
    }
}
