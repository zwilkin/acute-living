//
//  AdminDashVC.swift
//  Acute Living
//
//  Created by Zach on 12/11/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

public let SAVE_TENNANT_UNWIND: String = "SaveTennantUnwind"
class AdminDashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Unwind from New Tennant
    @IBAction func saveTennantUnwind(segue: UIStoryboardSegue) {
        
    }
    
    
    // MARK: - Actions & Outlets
    @IBAction func addNewTennant(_ sender: Any) {
        performSegue(withIdentifier: NEW_TENNANT_SEGUE, sender: sender)
    }
    @IBAction func logOut(_ sender: Any) {
        // Verify with user before logging out
        performSegue(withIdentifier: LOGOUT_UNWIND, sender: sender)
    }
    
}
