//
//  NewTennantVC.swift
//  Acute Living
//
//  Created by Zach on 12/11/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import Firebase

class NewTennantVC: UIViewController, UITextFieldDelegate {

    // Firebase Variables
    var auth: FIRAuth! = FIRAuth.auth()
    var database: FIRDatabase = FIRDatabase.database()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.enableKeyboardDismissOnTap()   // Cited in LoginVC
        // Set up the TextField Delegates
        for input in inputFields { input.delegate = self }
    }
    

    // MARK: - Text Field Functionality
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Check if it is the last input field
        if textField.tag == (inputFields.count - 1) { textField.resignFirstResponder() } // "Done"
        // Move to the next input field
        else { inputFields[textField.tag + 1].becomeFirstResponder() }
        return false
    }
    
    
    // MARK: - Actions & Outlets
    // Outlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var inputFields: [UITextField]!
    @IBOutlet weak var tempPassField: UITextField!
    @IBOutlet weak var verifyTempPassField: UITextField!
    // Actions
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveNewTennant(_ sender: Any) {
        // MARK: Pre-Save Error Checks
        // Check that both password fields are equal
        if tempPassField.text! != verifyTempPassField.text! {
            // Alert the user
            let alert = UIAlertController(title: "Password Mismatch", message: "Passwords do not match.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        // Check that all necessary fields are filled in
        for input in inputFields {
            if input.tag != 6   // Tag '6' is the "Street 2 (Optional)" input field
                && (input.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")).isEmpty)! {
                // A required field is empty - alert the user
                let alert = UIAlertController(title: "Empty Fields", message:
                    "One of more of the required fields are empty.\nPlease make sure all fields are filled in.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        // Check for valid internet conneciton before attempting any network operations
        let reachability = Reachability(hostname: "www.google.com")
        if reachability?.connection == Reachability.Connection.none {
            // No current internet connection - notify the user
            print("NO CONNECTION!!!")
            let alert = UIAlertController(title: "No Connection", message: "Connect to the internet and try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else {
            // Valid Internet Connection
            // Obtain variables to be saved to Firebase
            let email: String = inputFields[0].text!
            let password: String = inputFields[1].text!
            let property: String = inputFields[3].text!
            let unit: String = inputFields[4].text!
            let address: Address = Address(st1: inputFields[5].text!, st2: inputFields[6].text!, city: inputFields[7].text!,
                                           st: inputFields[8].text!, zip: Int(inputFields[9].text!)!)
            let rent: Double = Double(inputFields[10].text!)!
            
            // MARK: Save new tennant to Firebase
            // Turn on Activity Indicator
            Toggle.activityIndicator(on: true, indicator: self.activityIndicator)
            // 1. Create Authentication credentials
            auth.createUser(withEmail: email, password: password, completion: { (fbUser: FIRUser?, error) in
                // Check for errors
                if let e = error as NSError! {
                    // Create a placeholder alert
                    var alert = UIAlertController(title: "Placeholder", message: "This is a placeholder and should never be seen.", preferredStyle: .alert)
                    
                    // Creat the alert to be shown depending on what the error was
                    switch e.code {
                    case 17999: // Invalid Email Address
                        alert = UIAlertController(title: "Invalid Email Address", message: "Invalid email address entered", preferredStyle: .alert)
                        break
                    case 17007: // Email address already taken
                        alert = UIAlertController(title: "Email Exists", message: "An account with this email already exists. Please use a different email address.", preferredStyle: .alert)
                        break
                    case 17026: // Password must be at least 6 characters
                        alert = UIAlertController(title: "Invalid Password", message: "Password must be at least 6 characters long.", preferredStyle: .alert)
                        break
                    default: break
                    }
                    
                    // Turn off Activity Indicator
                    Toggle.activityIndicator(on: false, indicator: self.activityIndicator)
                    
                    // Add "Ok" to the alert and display it to the user
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    return
                }
                else {
                    // 2. Append to Database tree
                    FireHelper.saveNewTennant(property: property, unit: unit, address: address, rent: rent, user: fbUser!, vc: self)
                    
                    // Turn off Activity Indicator
                    Toggle.activityIndicator(on: false, indicator: self.activityIndicator)
                    
                    // Alert the user
                    let alert = UIAlertController(title: "Success", message: "\(unit) new tennant information has been saved.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        self.performSegue(withIdentifier: SAVE_TENNANT_UNWIND, sender: sender)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    print("Created new user #\(fbUser!.uid) with \(fbUser!.email!)")
                }
            })
        }
    }
    
    @IBAction func tempPassCheck(_ sender: UITextField) {
        // sender is 'Verify Temporary Password' field
        if tempPassField.text! != sender.text! { sender.textColor = UIColor.red }
        else { sender.textColor = UIColor.black }
    }
    
}
