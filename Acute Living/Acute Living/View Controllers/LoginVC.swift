//
//  LoginVC.swift
//  Acute Living
//
//  Created by Zach on 12/8/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import Firebase
import SystemConfiguration

// String Constants
public let LOGIN_SEGUE: String = "LoginSegue"
public let LOGOUT_UNWIND: String = "LogoutUnwind"
public let ADMIN_LOGIN_SEGUE: String = "AdminLoginSegue"
public let NEW_TENNANT_SEGUE: String = "NewTennantSegue"
public let NEW_SETUP_SEGUE: String = "NewSetupSegue"
public let SETUP_TO_DASHBOARD_SEGUE: String = "SetupToDashboardSegue"

class LoginVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Placeholder Variables
    public static var rememberMe: Bool = false
    
    // Admin
    public let AdminEmail: String = "admin@acute.com"
    
    // Firebase Variables
    var authRef: FIRAuth? = FIRAuth.auth()
    var authHandle: FIRAuthStateDidChangeListenerHandle!
    

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Enable dismissing the keyboard by tapping anywhere   ** CITATION AT THE BOTTOM **
        self.enableKeyboardDismissOnTap()
        
        // Set up the TextField delegates
        for input in inputFields { input.delegate = self }
        
        // Set up Firebase DB Reference variables
        authRef = FIRAuth.auth()
    }
    override func viewWillAppear(_ animated: Bool) {
        // Attach State Change Listener
        authHandle = authRef?.addStateDidChangeListener({ (auth: FIRAuth, fbUser: FIRUser?) in
            if fbUser != nil { print("USER STATE: LOGGED IN") }
            else { print("USER STATE: LOGGED OUT") }
        })
    }
    override func viewDidAppear(_ animated: Bool) {
        if FireHelper.userLoggedIn() {
            // Set the 'Remember Me' switch according to whether user is already logged in
            AutoLogin_Switch.setOn(true, animated: true)
            LoginVC.rememberMe = true
            // User remained logged in - check for admin
            if let fbUser = authRef?.currentUser {
                if fbUser.email == AdminEmail {
                    // Admin Acconut
                    self.performSegue(withIdentifier: ADMIN_LOGIN_SEGUE, sender: self)
                }
                else {
                    // User Account
                    self.performSegue(withIdentifier: LOGIN_SEGUE, sender: self)
                }
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        // Detatch State Change Listener
        authRef?.removeStateDidChangeListener(authHandle)
    }
    
    
    
    
    // Text Field Functionality
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Check if it is the last input field
        if textField.tag == (inputFields.count - 1) {
            // "Done"
            textField.resignFirstResponder()
            loginAttempt(self)
        }
            // Move to the next input field
        else { inputFields[textField.tag + 1].becomeFirstResponder() }
        return false
    }
    
    
    
    // MARK: - Unwind Segue
    @IBAction func logoutUnwindSegue(segue: UIStoryboardSegue) {
        FireHelper.signCurrentUserOut()                 // Sign out the user
        AutoLogin_Switch.setOn(false, animated: true)   // Disable 'Remember Me'
        for input in inputFields { input.text = "" }    // Clear input fields
        
        print("Successfully logged out.")
    }
    
    
    
    
    
    // MARK: - Outlets & Actions
    // Outlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var inputFields: [UITextField]!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet public weak var AutoLogin_Switch: UISwitch!
    // Actions
    @IBAction func switchChanged(_ sender: UISwitch) {
        LoginVC.rememberMe = sender.isOn // Update the local variable
    }
    @IBAction func loginAttempt(_ sender: Any) {
        // Check for valid internet conneciton before attempting any network operations
        let reachability = Reachability(hostname: "www.google.com")
        if reachability?.connection == Reachability.Connection.none {
            // No current internet connection - notify the user
            print("NO CONNECTION!!!")
            let alert = UIAlertController(title: "No Connection", message: "Connect to the internet and try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else {
            // Check if email and password fields are blank
            if emailField.text!.isEmpty || passwordField.text!.isEmpty {
                // Alert the user email & pass are required
                let alert = UIAlertController(title: "Empty Fields", message: "Username and password are required.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else {
                // Turn on Activity Indicator
                Toggle.activityIndicator(on: true, indicator: self.activityIndicator)
                
                // Attempt to log in with user-inputted details
                authRef?.signIn(withEmail: emailField.text!, password: passwordField.text!, completion: {
                    (user, error) in
                    // Error handling
                    if let e = error as NSError! {
                        // Login Failed
                        switch (e.code) {
                        case 17009, 17011:
                            // Invalid email and/or password
                            let alert = UIAlertController(title: "Login Failed!", message: "Invalid email address and/or password.\nPlease try again.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                            break
                        case 17999:
                            // Invalid email address format
                            let alert = UIAlertController(title: "Inalid Email!", message: "Email address is not properly formatted.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                            break
                        default: break
                        }
                        
                        print("\n\nERROR DEBUG:\n\(error.debugDescription)\n")
                        print("\\nLOCALIZED: \n\(e.localizedDescription)")
                    }
                    else {
                        // Turn off Activity Indicator
                        Toggle.activityIndicator(on: false, indicator: self.activityIndicator)
                        
                        // Check if account is Admin
                        if let fbUser = user as FIRUser! {
                            if fbUser.email! == self.AdminEmail {
                                // Admin Login
                                self.performSegue(withIdentifier: ADMIN_LOGIN_SEGUE, sender: self)
                            }
                            else {
                                // MARK: Check if first ever login
                                // Get the Database reference for the specific user (by uId)
                                let idRef = FIRDatabase.database().reference().child(FireHelper.USERS).child(fbUser.uid)
                                idRef.observeSingleEvent(of: .value, with: { (snapshot) in
                                    if let setupComplete: Bool = snapshot.childSnapshot(forPath: "setupComplete").value as? Bool {
                                        if (setupComplete) {
                                            self.performSegue(withIdentifier: LOGIN_SEGUE, sender: self)
                                        }
                                        else {
                                            // Account Setup
                                            let alert = UIAlertController(title: "Set Up", message: "This is your first time logging in to Acute Living so your account must be set up.", preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action) in
                                                // MARK: Navigate to setup on-boarding
                                                self.performSegue(withIdentifier: NEW_SETUP_SEGUE, sender: sender)
                                            }))
                                            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                                                // Logout
                                                FireHelper.signCurrentUserOut()                     // Sign out the user
                                                self.AutoLogin_Switch.setOn(false, animated: true)  // Disable 'Remember Me'
                                                for input in self.inputFields { input.text = "" }   // Clear input fields
                                            }))
                                            self.present(alert, animated: true, completion: { print("NEW TENNANT NEEDS TO BE SET UP - COMMENCE ONBOARDING") })
                                        }
                                    }
                                })
                            }
                        }
                    }
                })
            }
        }
    }
}

// MARK: - Extensions
/*
 *  EZSwiftExtensions
 *
 *  This code snippet was taken from the public EZSwiftExtensions Github Repository
 *  that is free for commercial and private use.
 *
 *  EZSwiftExtensions is licensed under the MIT License (MIT)
 *
 *  Source: https://github.com/goktugyil/EZSwiftExtensions/blob/master/Sources/UIViewControllerExtensions.swift
 *
 */
extension UIViewController {
    func enableKeyboardDismissOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
