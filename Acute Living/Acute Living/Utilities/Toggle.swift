//
//  ToggleUtil.swift
//  Acute Living
//
//  Created by Zach on 12/13/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

public class Toggle {
    
    // Activity Indicator
    public static func activityIndicator(on: Bool, indicator: UIActivityIndicatorView) {
        if on { indicator.startAnimating() }
        else { indicator.stopAnimating() }
    }
    
    
    // Toggle TextView Input Fields
    public static func inputFields(enabled: Bool, inputs textFields: [UITextView]) {
        for field in textFields {
            field.isUserInteractionEnabled = enabled
        }
    }
    
}
