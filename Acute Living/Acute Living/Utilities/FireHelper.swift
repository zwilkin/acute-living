//
//  FireHelper.swift
//  iOSApp
//
//  Created by Zach on 11/3/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import Foundation
import Firebase

public let USERS: String = "users"

public class FireHelper {
    
    // Constant String Identifier Variables
    public static let USERS: String = "users";
    public static let EMAIL: String = "email";
    public static let FIRST: String = "firstName";
    public static let LAST: String = "lastName"
    public static let PHONE: String = "phoneNumber"
    public static let PROPERTY: String = "property"
    public static let UNIT: String = "unit"
    
    
    
    /**
     Returns a Bool value on whether or not a Firebase user is currently logged in
     - returns: Bool
    */
    public static func userLoggedIn() -> Bool {
        return FIRAuth.auth()?.currentUser != nil
    }
    
    
    
    /**
     Save a new Tennant to the Firebase database
     - parameter user: Currently logged in FIRUser
     - parameter vc: Currently displayed ViewController
    */
    public static func saveNewTennant(property: String, unit: String, address: Address, rent: Double, user: FIRUser, vc: UIViewController) {
        // Get the database references for "users"
        let usersRef: FIRDatabaseReference = FIRDatabase.database().reference().child(USERS)
        
        // Create the new updates 
        let newTennantUpdates: [String: [String: Any]] =
            [user.uid:
                ["email": user.email!,
                 "setupComplete": false,
                 "pushNotifications": false,
                 "firstName": "",
                 "lastName": "",
                 "phoneNumber": false,
                 "property" : property,
                 "unit": unit,
                 "address": [
                    "street1": address.street1,
                    "street2": address.street2,
                    "city:": address.city,
                    "state": address.state,
                    "zip": address.zip],
                 "bills": [
                    "dueDate": "Dec 1, 2017",
                    "isLate": false,
                    "isPaid": false,
                    "lateFee": 0,
                    "parking": false,
                    "pestControl": 25.00,
                    "rent": rent,
                    "water": 0.00]
                ]]
        
        // Send the new user info update
        usersRef.updateChildValues(newTennantUpdates) { (error, reference) in
            if let error = error {
                // Error Handle
                print("\n" + error.localizedDescription)
                return
            }
        }
    }
    
    
    
    /**
     Update the currently signed in Firebase user with the given parameters.
     Email address is non-changeable
    */
    public static func updateUserInfo(uId: String, username: String, first: String, age: NSNumber, vc: UIViewController) {
        // Get the database references for the current user by the uId
        let usersRef: FIRDatabaseReference = FIRDatabase.database().reference().child(USERS).child(uId)
        
        // Create the new updates
        let updates: [String: Any] = [
            "username"  : username,
            "first"     : first,
            "age"       : age
        ]
        
        // Push the new user's updates to the database
        usersRef.updateChildValues(updates) { (error, reference) in
            if let error = error {
                // Error Handle
                print("\n" + error.localizedDescription)
            }
            else {
                // Display alert that changes were successfully saved
                let alert = UIAlertController(title: "Success!", message: "User information has been successfully updated.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                vc.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    
    /**
     Signs out the currently logged in Firebase user
    */
    public static func signCurrentUserOut() {
        do {
            try FIRAuth.auth()?.signOut()
        } catch let error as NSError {
            print("Error signing out: \(error)")
        }
    }
    
}
