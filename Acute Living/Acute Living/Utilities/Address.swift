//
//  User.swift
//  iOSApp
//
//  Created by Zach on 11/3/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import Foundation

public class Address {
    
    // Member Variables
    public var street1: String
    public var street2: String
    public var city: String
    public var state: String
    public var zip: Int
    
    // Default initializer for DataSnapshot
    init() {
        self.street1 = "123 Street Name"
        self.street2 = ""
        self.city = "City"
        self.state = "ST"
        self.zip = 12345
    }
    
    // User creation initializer
    init(st1 street1: String, st2 street2: String, city: String, st state: String, zip: Int) {
        self.street1 = street1
        self.street2 = street2
        self.city = city
        self.state = state
        self.zip = zip
    }
    
    
    // Setter methods
    func setStreet1(st1 street1: String) { self.street1 = street1}
    func setStreet2(st2 street2: String) { self.street2 = street2}
    func setCity(city: String) { self.city = city }
    func setState(st state: String) { self.state = state }
    func setZip(zip: Int) { self.zip = zip }
    
}
