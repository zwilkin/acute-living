# Acute Living #
---
Acute Living is an application that will incorporate everything a home renter will need such as easy bill paying and maintenance
services requests, all while allowing you to keep in contact with your landlord with real-time updates and notifications

Acute Living is a registered trademark of Acute Property Management (Denver, CO) and is privately owned, used, and deemed by 
Acute Property Management, including any and all subsidiaries.


## Version ##
#### Current Version: 1.0 ####
---

## ChangeLog ##
### December 1, 2017 ###
Database service has been moved to Firebase

---

### Developer Information ###
Developed by Zachary Wilkin - zwilkin@fullsail.edu
